import random

def count(seq):
    """Count the number of items in sequence that are interpreted as true."""
    return sum(map(bool, seq))


def first(iterable, default=None):
    """Return the first element of an iterable; or default."""
    return next(iter(iterable), default)


def extend(s, var, val):
    """Copy dict s and extend it by setting var to val; return copy."""
    try:  # Python 3.5 and later
        return eval('{**s, var: val}')
    except SyntaxError:  # Python 3.4
        s2 = s.copy()
        s2[var] = val
        return s2

identity = lambda x: x

def argmin_random_tie(seq, key=identity):
    """Return a minimum element of seq; break ties at random."""
    return min(shuffled(seq), key=key)


def shuffled(iterable):
    """Randomly shuffle a copy of iterable."""
    items = list(iterable)
    random.shuffle(items)
    return items

def is_in(elt, seq):
    """Similar to (elt in seq), but compares with 'is', not '=='."""
    return any(x is elt for x in seq)

def read_words():
    text_file = open("data/words.txt", "r", encoding="utf8", errors = 'ignore')
    words = text_file.readlines()
    words_valid = []
    #Remove whitespace and newline and add valid entries to new list
    for index, word in enumerate(words):
        new_word = words[index] = word.rstrip()
        if len(new_word) > 2 and len(new_word) < 7:
            words_valid.append(new_word)

    #shuffle words
    random.shuffle(words_valid)
    #reduce domain -> takes too long time when using algorithms
    words = words[0:2000]

    text_file.close()
    return words_valid
