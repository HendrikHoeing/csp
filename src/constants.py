# todo problem description

no_option = "None"

map_coloring = "Map Coloring"
map_coloring_australia = "Australia"
map_coloring_usa = "USA"
map_coloring_france = "France"
map_coloring_description = "The Map Coloring CSP consists of a map with several adjacent countries. The goal is to color each country with no 2 adjacent countries with the same color."

n_queens = "N-Queens"
n_queens_eight = "8-Queens"
n_queens_sixteen = "16-Queens"
n_queens_thirtytwo = "32-Queens"
n_queens_description = "The N-Queens CSP consists of a quadratic field with N to N squares. The goal is to place all N queens on the board without them reaching each other vertically, horizontally or diagonally."

sudoku = "Sudoku"
sudoku_easy = "Easy"
sudoku_hard = "Hard"
sudoku_description = "The Sudoku CSP consists of a quadratic field with randomly placed numbers in it. The goal is to insert numbers so that there are no duplicates for each row and column."

zebra_puzzle = "Zebra Puzzle"
zebra_puzzle_description = "The Zebra Puzzla CSP is a riddle with certain statements. The goal is to find out who drinks water and whom the zebra belongs to."

crossword = "Crossword"
crossword_letters_easy = "Letters (14 Words)"
crossword_letters_dict = "Letters (2000 Words)"
crossword_words_easy = "Words (14 Words)"
crossword_words_dict = "Words (2000 Words)"
crossword_description = "The Crossword CSP consists of a quadratic field with letters in it. The goal is to insert letters/words so that all squares in vertical and diagonal space match a word."

kakuro = "Kakuro"
kakuro_easy = "Easy"
kakuro_average = "Average"
kakuro_hard = "Hard"
kakuro_description = "The Kakuro CSP is like a crossword puzzle, but instead of letters it uses numbers and instead of words it uses sums. The goal is to match all sums by inserting numbers."

consistency_ac3 = "AC3"
consistency_ac3b = "AC3b"
consistency_ac4 = "AC4"


search_alg = "Algorithm:"
search_alg_backtrack = "Backtracking search"
search_alg_hillclimbing = "Hill climbing"

inference_forward_checking = "Forward Checking"
inference_mac = "Maintaining arc consistency (MAC)"

variable_selecting = "Variable selection:"
variable_selecting_first_unassigned = "First unassigned variable"
variable_selecting_mrv = "Minimum-remaining-values (MRV)"
variable_selecting_degree = "Degree heuristic"

domain_ordering = "Domain ordering:"
domain_ordering_unordered_domain_values = "Unordered domain values"
domain_ordering_unordered_lcv = "Least-constraining-values (LCV)"
