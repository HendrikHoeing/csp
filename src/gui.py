# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'csp_ui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from datetime import datetime
import traceback

from src import constants as c
from src import problems
from src import algorithms
from src import csp
from src import utils


class Ui_MainWindow(object):

    def __init__(self):
        self.problems_values = {c.map_coloring: [c.map_coloring_australia, c.map_coloring_usa, c.map_coloring_france],
                                c.zebra_puzzle: [c.no_option],
                                c.n_queens: [c.n_queens_eight, c.n_queens_sixteen, c.n_queens_thirtytwo],
                                c.sudoku: [c.sudoku_easy, c.sudoku_hard],

                                c.crossword: [c.crossword_letters_easy, c.crossword_letters_dict, c.crossword_words_easy, c.crossword_words_dict],
                                c.kakuro: [c.kakuro_easy, c.kakuro_average, c.kakuro_hard]}
        self.problems_descriptions = {c.map_coloring: c.map_coloring_description,
                                      c.n_queens: c.n_queens_description,
                                      c.sudoku: c.sudoku_description,
                                      c.zebra_puzzle: c.zebra_puzzle_description,
                                      c.crossword: c.crossword_description,
                                      c.kakuro: c.kakuro_description}
        self.search_alg_values = [
            c.search_alg_backtrack, c.search_alg_hillclimbing]
        self.searchalg = c.search_alg_backtrack
        self.consistency_values = [c.no_option, c.consistency_ac3,
                                   c.consistency_ac3b, c.consistency_ac4]
        self.inference_values = [c.no_option,
                                 c.inference_forward_checking, c.inference_mac]
        self.variable_selecting_values = [c.variable_selecting_first_unassigned,
                                          c.variable_selecting_mrv, c.variable_selecting_degree]
        self.domain_ordering_values = [
            c.domain_ordering_unordered_domain_values, c.domain_ordering_unordered_lcv]

        self.csp = problems.australia_csp
        self.consistency = None
        self.inference = algorithms.no_inference
        self.variable_selecting = algorithms.first_unassigned_variable
        self.domain_ordering = algorithms.unordered_domain_values

        self.init_csps()

        self.searching = False

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 700)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(9, 9, 1181, 61))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.consistency_TF = QtWidgets.QPlainTextEdit(
            self.horizontalLayoutWidget)
        self.consistency_TF.setReadOnly(True)
        self.consistency_TF.setObjectName("consistency_TF")
        self.gridLayout.addWidget(self.consistency_TF, 2, 0, 1, 1)
        self.inference_CB = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        self.inference_CB.setObjectName("inference_CB")
        self.gridLayout.addWidget(self.inference_CB, 2, 3, 1, 1)
        self.unassigned_variable_CB = QtWidgets.QComboBox(
            self.horizontalLayoutWidget)
        self.unassigned_variable_CB.setObjectName("unassigned_variable_CB")
        self.gridLayout.addWidget(self.unassigned_variable_CB, 2, 5, 1, 1)
        self.csp_TF = QtWidgets.QPlainTextEdit(self.horizontalLayoutWidget)
        self.csp_TF.setEnabled(True)
        self.csp_TF.setReadOnly(True)
        self.csp_TF.setObjectName("csp_TF")
        self.gridLayout.addWidget(self.csp_TF, 1, 0, 1, 1)
        self.consistency_CB = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        self.consistency_CB.setObjectName("consistency_CB")
        self.gridLayout.addWidget(self.consistency_CB, 2, 1, 1, 1)
        self.variant_TF = QtWidgets.QPlainTextEdit(self.horizontalLayoutWidget)
        self.variant_TF.setReadOnly(True)
        self.variant_TF.setObjectName("variant_TF")
        self.gridLayout.addWidget(self.variant_TF, 1, 2, 1, 1)
        self.problem_CB = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        self.problem_CB.setObjectName("problem_CB")
        self.gridLayout.addWidget(self.problem_CB, 1, 1, 1, 1)
        self.variant_CB = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        self.variant_CB.setObjectName("variant_CB")
        self.gridLayout.addWidget(self.variant_CB, 1, 3, 1, 1)

        self.searchalg_TF = QtWidgets.QPlainTextEdit(
            self.horizontalLayoutWidget)
        self.searchalg_TF.setReadOnly(True)
        self.searchalg_TF.setObjectName("searchalg_TF")
        self.gridLayout.addWidget(self.searchalg_TF, 1, 4, 1, 1)
        self.searchalg_CB = QtWidgets.QComboBox(self.horizontalLayoutWidget)
        self.searchalg_CB.setObjectName("searchalg_CB")
        self.gridLayout.addWidget(self.searchalg_CB, 1, 5, 1, 1)

        self.inference_TF = QtWidgets.QPlainTextEdit(
            self.horizontalLayoutWidget)
        self.inference_TF.setReadOnly(True)
        self.inference_TF.setObjectName("inference_TF")
        self.gridLayout.addWidget(self.inference_TF, 2, 2, 1, 1)
        self.domain_order_TF = QtWidgets.QTextEdit(self.horizontalLayoutWidget)
        self.domain_order_TF.setEnabled(True)
        self.domain_order_TF.setReadOnly(True)
        self.domain_order_TF.setObjectName("domain_order_TF")
        self.gridLayout.addWidget(self.domain_order_TF, 2, 6, 1, 1)
        self.select_variable_TF = QtWidgets.QPlainTextEdit(
            self.horizontalLayoutWidget)
        self.select_variable_TF.setReadOnly(True)
        self.select_variable_TF.setObjectName("select_variable_TF")
        self.gridLayout.addWidget(self.select_variable_TF, 2, 4, 1, 1)
        self.order_domain_values_CB = QtWidgets.QComboBox(
            self.horizontalLayoutWidget)
        self.order_domain_values_CB.setObjectName("order_domain_values_CB")
        self.gridLayout.addWidget(self.order_domain_values_CB, 2, 7, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        self.logging_TF = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.logging_TF.setGeometry(QtCore.QRect(10, 80, 1181, 461))
        self.logging_TF.setReadOnly(True)
        self.logging_TF.setPlainText("")
        self.logging_TF.setObjectName("logging_TF")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(870, 580, 321, 71))
        self.pushButton.setObjectName("pushButton")
        self.information_TF = QtWidgets.QTextEdit(self.centralwidget)
        self.information_TF.setGeometry(QtCore.QRect(10, 560, 671, 91))
        self.information_TF.setReadOnly(True)
        self.information_TF.setObjectName("information_TF")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1224, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        # Assign values
        self.problem_CB.addItems(self.problems_values)
        self.variant_CB.addItems(self.problems_values[c.map_coloring])

        self.searchalg_TF.setPlainText(c.search_alg)
        self.searchalg_CB.addItems(self.search_alg_values)

        self.consistency_CB.addItems(self.consistency_values)
        self.inference_CB.addItems(self.inference_values)
        self.unassigned_variable_CB.addItems(self.variable_selecting_values)
        self.order_domain_values_CB.addItems(self.domain_ordering_values)

        # Action listeners
        self.problem_CB.currentTextChanged.connect(self.on_problemCB_changed)
        self.variant_CB.currentTextChanged.connect(self.on_variantCB_changed)
        self.searchalg_CB.currentTextChanged.connect(
            self.on_searchalgCB_changed)

        self.consistency_CB.currentTextChanged.connect(
            self.on_consistencyCB_changed)
        self.inference_CB.currentTextChanged.connect(
            self.on_inferenceCB_changed)
        self.unassigned_variable_CB.currentTextChanged.connect(
            self.on_unassigned_variableCB_changed)
        self.order_domain_values_CB.currentTextChanged.connect(
            self.on_order_domainCB_changed)

        self.pushButton.clicked.connect(self.on_start_clicked)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def on_problemCB_changed(self, value):
        self.log_info(
            self.problems_descriptions[value])
        self.variant_CB.clear()
        self.variant_CB.addItems(self.problems_values[value])
        self.csp = self.csps[value+self.problems_values[value][0]]

    def on_variantCB_changed(self, value):
        if(value != ''):
            self.csp = self.csps[self.problem_CB.currentText()+value]
            try:
                self.log_info(f"Variant:\n{self.csp.display()}")
            except:
                pass
            
    def on_searchalgCB_changed(self, value):
        if(value != ''):
            self.searchalg = value

    def on_consistencyCB_changed(self, value):
        if(value == c.no_option):
            self.consistency = None
        elif(value == c.consistency_ac3):
            self.consistency = algorithms.AC3
        elif(value == c.consistency_ac3b):
            self.consistency = algorithms.AC3b
        elif(value == c.consistency_ac4):
            self.consistency = algorithms.AC4

    def on_inferenceCB_changed(self, value):
        if(value == c.no_option):
            self.inference = algorithms.no_inference
        elif(value == c.inference_forward_checking):
            self.inference = algorithms.forward_checking
        elif(value == c.inference_mac):
            self.inference = algorithms.mac

    def on_unassigned_variableCB_changed(self, value):
        if(value == c.variable_selecting_first_unassigned):
            self.variable_selecting = algorithms.first_unassigned_variable
        elif(value == c.variable_selecting_mrv):
            self.variable_selecting = algorithms.mrv
        elif(value == c.variable_selecting_degree):
            self.variable_selecting = algorithms.degree_heuristic

    def on_order_domainCB_changed(self, value):
        if(value == c.domain_ordering_unordered_domain_values):
            self.domain_ordering = algorithms.unordered_domain_values
        elif(value == c.domain_ordering_unordered_lcv):
            self.domain_ordering = algorithms.lcv

    def on_start_clicked(self):
        if(not self.searching):
            try:
                self.log_info("\n\nStart searching...")
                self.log_info(f'Time: {datetime.now().strftime("%H:%M:%S")}\n')
                self.log_info(
                    f"CSP:{self.problem_CB.currentText()} | Variant: {self.variant_CB.currentText()} | Search algorithm: {self.searchalg_CB.currentText()}")
                self.log_info(
                    f"Consistency: {self.consistency_CB.currentText()} | Inference: {self.inference_CB.currentText()} | Variable selection: {self.unassigned_variable_CB.currentText()} | Domain ordering: {self.order_domain_values_CB.currentText()}")

                self.pushButton.setText("Stop searching")
                self.searching = True
                # Start thread to not block gui
                self.thread = Worker(self, self.print_results)
                self.thread.start()

            except Exception as error:
                traceback.print_exc()
                self.information_TF.setText(str(error))
                self.search_stopped()
                self.log_info("Search stopped.")
                return
        else:
            self.search_stopped()
            self.log_info("Search stopped.")

    def search_stopped(self):
        self.pushButton.setText("Start searching")
        self.searching = False
        self.thread.terminate()
        # reinitialiaze csps to clear data
        self.init_csps()

    def print_results(self, result, info, time):
        for info_string in info:
            self.log_info(info_string)

        if result:
            self.log_info('\nResult:')

            try:
                self.log_info(
                    f'{self.csp.display(result)}')
            except:
                self.log_info(f'{result}')
        else:
            self.log_info('Search finished. No solution found...')

        self.log_info(f'Time: {time}')
        self.log_info(
            "----------------------------------------------------------------------------------------\n\n\n")
        self.search_stopped()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "CSP Solver"))
        self.consistency_TF.setPlainText(
            _translate("MainWindow", "Consistency:"))
        self.csp_TF.setPlainText(_translate("MainWindow", "CSP:"))
        self.variant_TF.setPlainText(_translate("MainWindow", "Variant:"))
        self.inference_TF.setPlainText(_translate("MainWindow", "Inference:"))
        self.domain_order_TF.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                                "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                                "p, li { white-space: pre-wrap; }\n"
                                                "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                                "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Domain ordering:</p></body></html>"))
        self.select_variable_TF.setPlainText(
            _translate("MainWindow", "Variable selection:"))
        self.pushButton.setText(_translate("MainWindow", "Start searching"))
        self.information_TF.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                               "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                               "p, li { white-space: pre-wrap; }\n"
                                               "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
                                               "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Welcome to the CSP solver!</p></body></html>"))

    def init_csps(self):
        self.csps = {
            c.map_coloring+c.map_coloring_australia: problems.australia_csp,
            c.map_coloring+c.map_coloring_usa: problems.usa_csp,
            c.map_coloring+c.map_coloring_france: problems.france_csp,

            c.zebra_puzzle+c.no_option: problems.Zebra(),

            c.n_queens+c.n_queens_eight: problems.NQueensCSP(8),
            c.n_queens+c.n_queens_sixteen: problems.NQueensCSP(16),
            c.n_queens+c.n_queens_thirtytwo: problems.NQueensCSP(32),

            c.sudoku+c.sudoku_easy: problems.Sudoku(problems.easy1),
            c.sudoku+c.sudoku_hard: problems.Sudoku(problems.harder1),
            c.crossword+c.crossword_letters_easy: problems.Crossword(problems.crossword1, problems.words1),
            c.crossword+c.crossword_letters_dict: problems.Crossword(problems.crossword1, utils.read_words()),
            c.crossword+c.crossword_words_easy: problems.Crossword_Words(problems.crossword1, problems.words1),
            c.crossword+c.crossword_words_dict: problems.Crossword_Words(problems.crossword1, utils.read_words()),

            c.kakuro+c.kakuro_easy: problems.Kakuro(problems.kakuro1),
            c.kakuro+c.kakuro_average: problems.Kakuro(problems.kakuro3),
            c.kakuro+c.kakuro_hard: problems.Kakuro(problems.kakuro4),
        }
        # throws error when programs start since comboboxes are not initialized yet
        # used to reinitialize csps after search to reset data
        try:
            self.csp = self.csps[self.problem_CB.currentText(
            )+self.variant_CB.currentText()]
        except:
            pass

    # function to return key for any value
    def get_key(self, my_dict, val):
        for key, value in my_dict.items():
            if val == value:
                return key
        return "key doesn't exist"

    def log_info(self, info):
        self.logging_TF.ensureCursorVisible()
        self.logging_TF.insertPlainText('\n' +
                                        info + '\n')
        self.logging_TF.ensureCursorVisible()


def main():
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


class Worker(QtCore.QThread):

    output = QtCore.pyqtSignal(dict, list, str)

    def __init__(self, gui, callback):
        super(Worker, self).__init__()
        self.gui = gui
        self.output.connect(callback)

    def run(self):
        result, info, time = self.gui.csp.solve(self.gui.csp, self.gui.consistency, self.gui.searchalg, self.gui.variable_selecting,
                                                self.gui.domain_ordering, self.gui.inference)

        self.output.emit(result or {}, info, time)
