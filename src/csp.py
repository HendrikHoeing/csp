"""CSP (Constraint Satisfaction Problems) problems and solvers. (Chapter 6)"""

import itertools
import random
import re
import string
from collections import defaultdict, Counter
from functools import reduce
from operator import eq, neg
import time

from sortedcontainers import SortedSet

from src import utils
from src import algorithms
from src import csp
from src import constants as c
from src import problems


class CSP():
    """This class describes finite-domain Constraint Satisfaction Problems.
    A CSP is specified by the following inputs:
        variables   A list of variables; each is atomic (e.g. int or string).
        domains     A dict of {var:[possible_value, ...]} entries.
        neighbors   A dict of {var:[var,...]} that for each variable lists
                    the other variables that participate in constraints.
        constraints A function f(A, a, B, b) that returns true if neighbors
                    A, B satisfy the constraint when they have values A=a, B=b

    In the textbook and in most mathematical definitions, the
    constraints are specified as explicit pairs of allowable values,
    but the formulation here is easier to express and more compact for
    most cases (for example, the n-Queens problem can be represented
    in O(n) space using this notation, instead of O(n^4) for the
    explicit representation). In terms of describing the CSP as a
    problem, that's all there is.

    However, the class also supports data structures and methods that help you
    solve CSPs by calling a search function on the CSP. Methods and slots are
    as follows, where the argument 'a' represents an assignment, which is a
    dict of {var:val} entries:
        assign(var, val, a)     Assign a[var] = val; do other bookkeeping
        unassign(var, a)        Do del a[var], plus other bookkeeping
        nconflicts(var, val, a) Return the number of other variables that
                                conflict with var=val
        curr_domains[var]       Slot: remaining consistent values for var
                                Used by constraint propagation routines.
    The following methods are used only by graph_search and tree_search:
        actions(state)          Return a list of actions
        result(state, action)   Return a successor of state
        goal_test(state)        Return true if all constraints satisfied
    The following are just for debugging purposes:
        nassigns                Slot: tracks the number of assignments made
        display(a)              Print a human-readable representation
    """

    def __init__(self, variables, domains, neighbors, constraints):
        """Construct a CSP problem. If variables is empty, it becomes domains.keys()."""
        variables = variables or list(domains.keys())
        self.variables = variables
        self.domains = domains
        self.neighbors = neighbors
        self.constraints = constraints
        self.curr_domains = None
        self.nassigns = 0

    def assign(self, var, val, assignment):
        """Add {var: val} to assignment; Discard the old value if any."""
        assignment[var] = val
        self.nassigns += 1

    def unassign(self, var, assignment):
        """Remove {var: val} from assignment.
        DO NOT call this if you are changing a variable to a new value;
        just call assign for that."""
        if var in assignment:
            del assignment[var]

    def nconflicts(self, var, val, assignment):  # modified 
        """Return the number of conflicts var=val has with other variables."""  
        conflicts = 0

        if callable(self.constraints):
            def conflict(var2):
                return var2 in assignment and not self.constraints(var, val, var2, assignment[var2])
            conflicts = utils.count(conflict(v) for v in self.neighbors[var])
        else:
            #temporarily assign var val to check conflicts
            # with hillclimbing all values are already assigned -> use bool to prevent deletion of assignments down below
            already_assigned = False 

            if var in assignment:
                already_assigned = True
            else:
                assignment[var] = val

            conflicts = utils.count(not con.holds(assignment)
                                    for con in self.constraints
                                    if all(v in assignment for v in con.scope))
            if not already_assigned:
                if var in assignment:
                    del assignment[var]

        return conflicts

    # checks if assignment is consistent
    def consistent(self, var, val, assignment):  # modified
        """assignment is a variable:value dictionary
        returns True if all of the constraints that can be evaluated
                        evaluate to True given assignment.
        """
        # Can either be list of constraints or constraint function
        # Done to generalize consistency function for all CSPs
        consistent = True

        if callable(self.constraints):
            def conflict(var2):
                return var2 in assignment and not self.constraints(var, val, var2, assignment[var2])

            for v in self.neighbors[var]:
                if conflict(v):
                    consistent = False
            return consistent
        else:
            #temporarily assign var val to check consistency
            assignment[var] = val
            consistent = all(con.holds(assignment)
                             for con in self.constraints
                             if all(v in assignment for v in con.scope))
            if var in assignment:
                del assignment[var]
        
        return consistent

    # generalized way to check if arc is consistent
    def arc_consistent(self, var=None, val=None, var2=None, val2=None):
        # Can either be list of constraints or constraint function
        # Done to generalize consistency function for all CSPs
        if(callable(self.constraints)):
            return self.constraints(var, val, var2, val2)
        else:
            # create temporary assignment end check its consistency
            assignment = {}
            assignment[var] = val
            assignment[var2] = val2

            return all(con.holds(assignment)
                       for con in self.constraints
                       if all(v in assignment for v in con.scope))

    # These are for constraint propagation
    def support_pruning(self):
        """Make sure we can prune values from domains. (We want to pay
        for this only if we use it.)"""
        if self.curr_domains is None:
            self.curr_domains = {
                v: list(self.domains[v]) for v in self.variables}

    def suppose(self, var, value):
        """Start accumulating inferences from assuming var=value."""
        self.support_pruning()
        removals = [(var, a) for a in self.curr_domains[var] if a != value]
        self.curr_domains[var] = [value]
        return removals

    def prune(self, var, value, removals):
        """Rule out var=value."""
        self.curr_domains[var].remove(value)
        if removals is not None:
            removals.append((var, value))

    def choices(self, var):
        """Return all values for var that aren't currently ruled out."""
        return (self.curr_domains or self.domains)[var]

    def infer_assignment(self):
        """Return the partial assignment implied by the current inferences."""
        self.support_pruning()
        return {v: self.curr_domains[v][0]
                for v in self.variables if 1 == len(self.curr_domains[v])}

    def restore(self, removals):
        """Undo a supposition and all inferences from it."""
        for B, b in removals:
            self.curr_domains[B].append(b)

    # This is for min_conflicts search

    def conflicted_vars(self, current):
        """Return a list of variables in current assignment that are in conflict"""
        return [var for var in self.variables
                if self.nconflicts(var, current[var], current) > 0]

    def solve(self, csp, consistency, searchalg=c.search_alg_backtrack,
              select_variable=algorithms.first_unassigned_variable,
              order_domain_values=algorithms.unordered_domain_values, inference=algorithms.no_inference):
        info = []
        start_time = time.time()
        # Enforce consistency
        if(consistency != None):
            consistency(csp, queue=None, removals=None,
                        arc_heuristic=algorithms.dom_j_up)
        if(searchalg == c.search_alg_backtrack):
            # solve by backtracking
            result = algorithms.backtracking_search(
                csp, select_variable, order_domain_values, inference)
        elif(searchalg == c.search_alg_hillclimbing):
            iterations = 100000
            result = algorithms.min_conflicts(csp, iterations)
            if not result:
                info.append(f"No solution found after {iterations} iterations")

        return result, info, "--- %s seconds ---" % (time.time() - start_time)
